﻿using System;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.WebHost;
using System.Threading.Tasks;


namespace GrpcClient
{
    class Program
    {
        private static async Task Main()
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");

            var client = new RpcCustomerService.RpcCustomerServiceClient(channel);

            var reply = client.GetCustomers(new EmptyResponse());
            var id = reply.Customers[0].Id;
           
            //test read all
            Console.WriteLine("read all get first:"+reply.Customers[0].Id);

            //test read by id
            var reply2 = client.GetCustomer(new CustomerQuery() { Id = id.ToString() });
            Console.WriteLine("read by id:" + reply2.Id);

            //test read by id
            var reply3 = client.CreateCustomer(new NewCustomer() { FirstName = "test", LastName="test",Email="test",Prefernces= new Prefernces()});
            Console.WriteLine("create"+reply3.Id);

            //delete
            var reply4 = client.DeleteCustomer(new CustomerQuery() { Id = id.ToString() });
            Console.WriteLine("deleted");

            //confirm delete
            var reply6 = client.GetCustomer(new CustomerQuery() { Id = id.ToString() });
            Console.WriteLine("check deleted:" + reply6.Id);

            Console.Read();
        }
    }
}
