﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Application
{
    public interface ICustomerService
    {
        Task<IEnumerable<Customer>> GetCustomersAsync();
        
        Task<Customer> GetCustomerAsync(Guid id);

        Task DeleteAsync(Guid id);

        Task UpdateAsync(Guid id, CreateOrEditCustomerRequest request);

        Task<Customer> CreateAsync(CreateOrEditCustomerRequest request);
    }
}
