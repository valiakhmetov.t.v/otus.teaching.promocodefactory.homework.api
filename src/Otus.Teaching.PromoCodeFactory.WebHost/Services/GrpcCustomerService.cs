﻿using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.WebHost;
using Otus.Teaching.PromoCodeFactory.Core.Application;
using System;
using Otus.Teaching.PromoCodeFactory.Core.Dto;

namespace Otus.GrpcServer.Services
{

    public class GrpcCustomerService : RpcCustomerService.RpcCustomerServiceBase
    {
       
        private readonly ICustomerService _customerService;

        public GrpcCustomerService(ICustomerService svc)
        {
            _customerService = svc;
        }
       
        public override  Task<SingleCustomerResponse> GetCustomer(CustomerQuery request, ServerCallContext context)
        {
            var result =   _customerService.GetCustomerAsync(new Guid(request.Id)).Result;
            
            SingleCustomerResponse response = new SingleCustomerResponse();
            response.Id = result.Id.ToString();
            response.FirstName = result.FirstName.ToString();
            response.LastName = result.LastName.ToString();
            response.Email = result.Email.ToString();

            return  Task.FromResult(response);
        }


        public override  Task<MultipleCustomersResponse> GetCustomers(EmptyResponse request, ServerCallContext context)
        {
            var result =  _customerService.GetCustomersAsync().Result;
            MultipleCustomersResponse customersResponse = new MultipleCustomersResponse();

            foreach (var customer in result)
            {
                SingleCustomerResponse customerResponse = new SingleCustomerResponse();
                customerResponse.Id = customer.Id.ToString();
                customerResponse.FirstName = customer.FirstName.ToString();
                customerResponse.LastName = customer.LastName.ToString();
                customerResponse.Email = customer.Email.ToString();
                customersResponse.Customers.Add(customerResponse);
            }

            return  Task.FromResult(customersResponse);
        }
        
        public override  Task<EmptyResponse> DeleteCustomer(CustomerQuery request, ServerCallContext context)
        {
             _customerService.DeleteAsync(new Guid(request.Id)).Wait();

            return Task.FromResult(new EmptyResponse());
        }

        public override Task<CustomerQuery> CreateCustomer(NewCustomer request, ServerCallContext context)
        {
            CreateOrEditCustomerRequest dto = new CreateOrEditCustomerRequest();
            dto.FirstName = request.FirstName;
            dto.LastName = request.LastName;
            dto.Email = request.Email;
            dto.PreferenceIds = new System.Collections.Generic.List<Guid>();
            foreach (var prefId in request.Prefernces.Ids)
            {
                dto.PreferenceIds.Add(new Guid(prefId));
            }

            var result = _customerService.CreateAsync(dto);

            return Task.FromResult(new CustomerQuery() { Id = result.Result.Id.ToString() });
        }
    }
}